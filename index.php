<?php

	require_once 'dbconfig.php';

	if(isset($_GET['delete_id']))
	{
		// select image from db to delete
		$stmt_select = $DB_con->prepare('SELECT book_pic FROM book WHERE book_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_id']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("user_images/".$imgRow['userPic']);

		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM db_data WHERE book_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_id']);
		$stmt_delete->execute();

		header("Location: index.php");
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
<title>Book2U</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
</head>

<body>
<div class="container">
<div class="navbar navbar-default navbar-static-top" role="navigation">

        <div class="navbar-header">
            <a class="navbar-brand" href="register.php">Register Seller</a>
            <a class="navbar-brand" href="Book2U_login.php">Login</a>
        </div>

    </div>


	<div class="page-header">

		<h1> Welcome To </h1>
			<img src="pic/Book2U.png" width="357" height="116" alt=""/>

	</div>

<div class="row">
<?php

	$stmt = $DB_con->prepare('SELECT book_id, book_name, book_author, book_pic FROM book ORDER BY book_id DESC');
	$stmt->execute();

	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>
			<div class="col-xs-3">
				<p class="page-header"><?php echo $book_name."&nbsp;/&nbsp;".$book_author; ?></p>
				<img src="user_images/<?php echo $row['book_pic']; ?>" class="img-rounded" width="250px" height="250px" />
				<p class="page-header">
				<span>
				<a class="btn btn-info" href="openform.php?edit_id=<?php echo $row['book_id']; ?>" title="click for open" onclick="return confirm('sure to open ?')"><span class="glyphicon glyphicon-edit"></span> Open</a>
				</span>
				</p>
			</div>
			<?php
		}
	}
	else
	{
		?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
            </div>
        </div>
        <?php
	}

?>
</div>



<div class="alert alert-info">
    <strong>Book2U™ establish 2016 </strong>
</div>

</div>


<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>


</body>
</html>
